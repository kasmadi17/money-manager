package com.cxteam.moneymanager.data;

public class User {
    private String uId;
    private String name;

    public String getuId(){
        return uId;
    }

    public void setuId(String uId){
        this.uId=uId;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }
}
